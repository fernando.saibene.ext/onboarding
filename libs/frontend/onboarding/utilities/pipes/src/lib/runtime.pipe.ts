import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'runtime'
})
export class RuntimePipe implements PipeTransform {

  transform(value: number): string {
    const hours = value / 60; //since both are ints, you get an int
    const minutes = value % 60;
    return `${hours}h ${minutes}m`;
  }
}
